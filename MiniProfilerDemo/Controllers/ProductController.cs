﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MiniProfilerDemo.Models;
using StackExchange.Profiling;

namespace MiniProfilerDemo.Controllers
{
    public class ProductController : Controller
    {
        public ActionResult Index()
        {
            using (EFDbContext db = new EFDbContext())
            {
                var profiler = MiniProfiler.Current;
                List<Product> m;
                using (profiler.Step("Product列表"))
                {
                    m = db.Products.ToList();
                }
                return View(m);
            }
        }
    }
}