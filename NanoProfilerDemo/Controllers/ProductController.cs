﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using EF.Diagnostics.Profiling;
using MiniProfilerDemo.Models;

namespace MiniProfilerDemo.Controllers
{
    public class ProductController : Controller
    {
        public ActionResult Index()
        {
            using (EFDbContext db = new EFDbContext())
            {
                using (ProfilingSession.Current.Step("Product:"))
                {
                    List<Product> m;
                    using (ProfilingSession.Current.Step(() => "Get Data"))
                    {
                        m = db.Products.ToList();
                    }
                    return View(m);
                }
            }
        }

        protected override void Execute(RequestContext requestContext)
        {
            //using(ProfilingSession.Current.Step("Action Excute:"))
            base.Execute(requestContext);
        }
    }
}